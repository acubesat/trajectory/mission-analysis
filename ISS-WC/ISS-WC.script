%General Mission Analysis Tool(GMAT) Script
%Created: 2020-08-30 00:05:28


%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft DefaultSC;
GMAT DefaultSC.DateFormat = UTCGregorian;
GMAT DefaultSC.Epoch = '01 Jul 2023 12:00:00.000';
GMAT DefaultSC.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultSC.DisplayStateType = Keplerian;
GMAT DefaultSC.SMA = 6804.517952999998;
GMAT DefaultSC.ECC = 0.001337000000000274;
GMAT DefaultSC.INC = 51.53800000000001;
GMAT DefaultSC.RAAN = 259.702;
GMAT DefaultSC.AOP = 65.60400000000702;
GMAT DefaultSC.TA = 292.687999999993;
GMAT DefaultSC.DryMass = 3.89134;
GMAT DefaultSC.Cd = 2.2;
GMAT DefaultSC.Cr = 1.2;
GMAT DefaultSC.DragArea = 0.03142;
GMAT DefaultSC.SRPArea = 0.1562;
GMAT DefaultSC.SPADDragScaleFactor = 1;
GMAT DefaultSC.SPADSRPScaleFactor = 1;
GMAT DefaultSC.NAIFId = -10000001;
GMAT DefaultSC.NAIFIdReferenceFrame = -9000001;
GMAT DefaultSC.OrbitColor = Red;
GMAT DefaultSC.TargetColor = Teal;
GMAT DefaultSC.OrbitErrorCovariance = [ 1e+70 0 0 0 0 0 ; 0 1e+70 0 0 0 0 ; 0 0 1e+70 0 0 0 ; 0 0 0 1e+70 0 0 ; 0 0 0 0 1e+70 0 ; 0 0 0 0 0 1e+70 ];
GMAT DefaultSC.CdSigma = 1e+70;
GMAT DefaultSC.CrSigma = 1e+70;
GMAT DefaultSC.Id = 'SatId';
GMAT DefaultSC.Attitude = CoordinateSystemFixed;
GMAT DefaultSC.SPADSRPInterpolationMethod = Bilinear;
GMAT DefaultSC.SPADSRPScaleFactorSigma = 1e+70;
GMAT DefaultSC.SPADDragInterpolationMethod = Bicubic;
GMAT DefaultSC.SPADDragScaleFactorSigma = 1e+70;
GMAT DefaultSC.ModelFile = 'aura.3ds';
GMAT DefaultSC.ModelOffsetX = 0;
GMAT DefaultSC.ModelOffsetY = 0;
GMAT DefaultSC.ModelOffsetZ = 0;
GMAT DefaultSC.ModelRotationX = 0;
GMAT DefaultSC.ModelRotationY = 0;
GMAT DefaultSC.ModelRotationZ = 0;
GMAT DefaultSC.ModelScale = 1;
GMAT DefaultSC.AttitudeDisplayStateType = 'Quaternion';
GMAT DefaultSC.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT DefaultSC.AttitudeCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultSC.EulerAngleSequence = '321';

%----------------------------------------
%---------- GroundStations
%----------------------------------------

Create GroundStation GroundStation30;
GMAT GroundStation30.OrbitColor = Thistle;
GMAT GroundStation30.TargetColor = DarkGray;
GMAT GroundStation30.CentralBody = Earth;
GMAT GroundStation30.StateType = Spherical;
GMAT GroundStation30.HorizonReference = Sphere;
GMAT GroundStation30.Location1 = 40.627233;
GMAT GroundStation30.Location2 = 22.959887;
GMAT GroundStation30.Location3 = 0.056;
GMAT GroundStation30.Id = 'StationId';
GMAT GroundStation30.IonosphereModel = 'None';
GMAT GroundStation30.TroposphereModel = 'None';
GMAT GroundStation30.DataSource = 'Constant';
GMAT GroundStation30.Temperature = 295.1;
GMAT GroundStation30.Pressure = 1013.5;
GMAT GroundStation30.Humidity = 55;
GMAT GroundStation30.MinimumElevationAngle = 30;

Create GroundStation GroundStation20;
GMAT GroundStation20.OrbitColor = Thistle;
GMAT GroundStation20.TargetColor = DarkGray;
GMAT GroundStation20.CentralBody = Earth;
GMAT GroundStation20.StateType = Spherical;
GMAT GroundStation20.HorizonReference = Sphere;
GMAT GroundStation20.Location1 = 40.627233;
GMAT GroundStation20.Location2 = 22.959887;
GMAT GroundStation20.Location3 = 0.056;
GMAT GroundStation20.Id = 'StationId';
GMAT GroundStation20.IonosphereModel = 'None';
GMAT GroundStation20.TroposphereModel = 'None';
GMAT GroundStation20.DataSource = 'Constant';
GMAT GroundStation20.Temperature = 295.1;
GMAT GroundStation20.Pressure = 1013.5;
GMAT GroundStation20.Humidity = 55;
GMAT GroundStation20.MinimumElevationAngle = 20;





%----------------------------------------
%---------- ForceModels
%----------------------------------------

Create ForceModel DefaultProp_ForceModel;
GMAT DefaultProp_ForceModel.CentralBody = Earth;
GMAT DefaultProp_ForceModel.PrimaryBodies = {Earth};
GMAT DefaultProp_ForceModel.PointMasses = {Sun};
GMAT DefaultProp_ForceModel.SRP = On;
GMAT DefaultProp_ForceModel.RelativisticCorrection = Off;
GMAT DefaultProp_ForceModel.ErrorControl = RSSStep;
GMAT DefaultProp_ForceModel.GravityField.Earth.Degree = 4;
GMAT DefaultProp_ForceModel.GravityField.Earth.Order = 4;
GMAT DefaultProp_ForceModel.GravityField.Earth.StmLimit = 100;
GMAT DefaultProp_ForceModel.GravityField.Earth.PotentialFile = 'JGM2.cof';
GMAT DefaultProp_ForceModel.GravityField.Earth.TideModel = 'None';
GMAT DefaultProp_ForceModel.SRP.Flux = 1367;
GMAT DefaultProp_ForceModel.SRP.SRPModel = Spherical;
GMAT DefaultProp_ForceModel.SRP.Nominal_Sun = 149597870.691;
GMAT DefaultProp_ForceModel.Drag.AtmosphereModel = MSISE90;
GMAT DefaultProp_ForceModel.Drag.HistoricWeatherSource = 'ConstantFluxAndGeoMag';
GMAT DefaultProp_ForceModel.Drag.PredictedWeatherSource = 'ConstantFluxAndGeoMag';
GMAT DefaultProp_ForceModel.Drag.CSSISpaceWeatherFile = 'SpaceWeather-All-v1.2.txt';
GMAT DefaultProp_ForceModel.Drag.SchattenFile = 'SchattenPredict.txt';
GMAT DefaultProp_ForceModel.Drag.F107 = 150;
GMAT DefaultProp_ForceModel.Drag.F107A = 150;
GMAT DefaultProp_ForceModel.Drag.MagneticIndex = 3;
GMAT DefaultProp_ForceModel.Drag.SchattenErrorModel = 'Nominal';
GMAT DefaultProp_ForceModel.Drag.SchattenTimingModel = 'NominalCycle';
GMAT DefaultProp_ForceModel.Drag.DragModel = 'Spherical';

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator DefaultProp;
GMAT DefaultProp.FM = DefaultProp_ForceModel;
GMAT DefaultProp.Type = RungeKutta89;
GMAT DefaultProp.InitialStepSize = 60;
GMAT DefaultProp.Accuracy = 9.999999999999999e-12;
GMAT DefaultProp.MinStep = 0.001;
GMAT DefaultProp.MaxStep = 2700;
GMAT DefaultProp.MaxStepAttempts = 50;
GMAT DefaultProp.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- EventLocators
%----------------------------------------

Create EclipseLocator EclipseLocator1;
GMAT EclipseLocator1.Spacecraft = DefaultSC;
GMAT EclipseLocator1.Filename = 'C:\Users\ACUBESAT-01\Desktop\Subsystems\TRA\ISS-WC\ISS-WC-E.txt';
GMAT EclipseLocator1.OccultingBodies = {Earth, Luna};
GMAT EclipseLocator1.InputEpochFormat = 'TAIModJulian';
GMAT EclipseLocator1.InitialEpoch = '21545';
GMAT EclipseLocator1.StepSize = 10;
GMAT EclipseLocator1.FinalEpoch = '21545.138';
GMAT EclipseLocator1.UseLightTimeDelay = true;
GMAT EclipseLocator1.UseStellarAberration = true;
GMAT EclipseLocator1.WriteReport = true;
GMAT EclipseLocator1.RunMode = Manual;
GMAT EclipseLocator1.UseEntireInterval = true;
GMAT EclipseLocator1.EclipseTypes = {'Umbra', 'Penumbra', 'Antumbra'};

Create ContactLocator ContactLocator30;
GMAT ContactLocator30.Target = DefaultSC;
GMAT ContactLocator30.Filename = 'C:\Users\ACUBESAT-01\Desktop\Subsystems\TRA\ISS-WC\ISS-WC-C-S.txt';
GMAT ContactLocator30.OccultingBodies = {Earth};
GMAT ContactLocator30.InputEpochFormat = 'TAIModJulian';
GMAT ContactLocator30.InitialEpoch = '21545';
GMAT ContactLocator30.StepSize = 10;
GMAT ContactLocator30.FinalEpoch = '21545.138';
GMAT ContactLocator30.UseLightTimeDelay = true;
GMAT ContactLocator30.UseStellarAberration = true;
GMAT ContactLocator30.WriteReport = true;
GMAT ContactLocator30.RunMode = Manual;
GMAT ContactLocator30.UseEntireInterval = true;
GMAT ContactLocator30.Observers = {GroundStation30};
GMAT ContactLocator30.LightTimeDirection = Transmit;

Create ContactLocator ContactLocator20;
GMAT ContactLocator20.Target = DefaultSC;
GMAT ContactLocator20.Filename = 'C:\Users\ACUBESAT-01\Desktop\Subsystems\TRA\ISS-WC\ISS-WC-C-UHF.txt';
GMAT ContactLocator20.OccultingBodies = {Earth};
GMAT ContactLocator20.InputEpochFormat = 'TAIModJulian';
GMAT ContactLocator20.InitialEpoch = '21545';
GMAT ContactLocator20.StepSize = 10;
GMAT ContactLocator20.FinalEpoch = '21545.138';
GMAT ContactLocator20.UseLightTimeDelay = true;
GMAT ContactLocator20.UseStellarAberration = true;
GMAT ContactLocator20.WriteReport = true;
GMAT ContactLocator20.RunMode = Manual;
GMAT ContactLocator20.UseEntireInterval = true;
GMAT ContactLocator20.Observers = {GroundStation20};
GMAT ContactLocator20.LightTimeDirection = Transmit;

%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create OrbitView DefaultOrbitView;
GMAT DefaultOrbitView.SolverIterations = Current;
GMAT DefaultOrbitView.UpperLeft = [ 0.004098360655737705 0 ];
GMAT DefaultOrbitView.Size = [ 0.5 0.45 ];
GMAT DefaultOrbitView.RelativeZOrder = 28;
GMAT DefaultOrbitView.Maximized = false;
GMAT DefaultOrbitView.Add = {DefaultSC, Earth};
GMAT DefaultOrbitView.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.DrawObject = [ true true ];
GMAT DefaultOrbitView.DataCollectFrequency = 1;
GMAT DefaultOrbitView.UpdatePlotFrequency = 50;
GMAT DefaultOrbitView.NumPointsToRedraw = 0;
GMAT DefaultOrbitView.ShowPlot = true;
GMAT DefaultOrbitView.MaxPlotPoints = 20000;
GMAT DefaultOrbitView.ShowLabels = true;
GMAT DefaultOrbitView.ViewPointReference = Earth;
GMAT DefaultOrbitView.ViewPointVector = [ 30000 0 0 ];
GMAT DefaultOrbitView.ViewDirection = Earth;
GMAT DefaultOrbitView.ViewScaleFactor = 1;
GMAT DefaultOrbitView.ViewUpCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.ViewUpAxis = Z;
GMAT DefaultOrbitView.EclipticPlane = Off;
GMAT DefaultOrbitView.XYPlane = On;
GMAT DefaultOrbitView.WireFrame = Off;
GMAT DefaultOrbitView.Axes = On;
GMAT DefaultOrbitView.Grid = Off;
GMAT DefaultOrbitView.SunLine = Off;
GMAT DefaultOrbitView.UseInitialView = On;
GMAT DefaultOrbitView.StarCount = 7000;
GMAT DefaultOrbitView.EnableStars = On;
GMAT DefaultOrbitView.EnableConstellations = On;

Create GroundTrackPlot DefaultGroundTrackPlot;
GMAT DefaultGroundTrackPlot.SolverIterations = Current;
GMAT DefaultGroundTrackPlot.UpperLeft = [ 0.004098360655737705 0.4560606060606061 ];
GMAT DefaultGroundTrackPlot.Size = [ 0.5 0.45 ];
GMAT DefaultGroundTrackPlot.RelativeZOrder = 32;
GMAT DefaultGroundTrackPlot.Maximized = false;
GMAT DefaultGroundTrackPlot.Add = {DefaultSC};
GMAT DefaultGroundTrackPlot.DataCollectFrequency = 1;
GMAT DefaultGroundTrackPlot.UpdatePlotFrequency = 50;
GMAT DefaultGroundTrackPlot.NumPointsToRedraw = 0;
GMAT DefaultGroundTrackPlot.ShowPlot = true;
GMAT DefaultGroundTrackPlot.MaxPlotPoints = 20000;
GMAT DefaultGroundTrackPlot.CentralBody = Earth;
GMAT DefaultGroundTrackPlot.TextureMap = 'ModifiedBlueMarble.jpg';

Create ReportFile ReportFile1;
GMAT ReportFile1.SolverIterations = Current;
GMAT ReportFile1.UpperLeft = [ 0 0 ];
GMAT ReportFile1.Size = [ 0 0 ];
GMAT ReportFile1.RelativeZOrder = 0;
GMAT ReportFile1.Maximized = false;
GMAT ReportFile1.Filename = 'C:\Users\ACUBESAT-01\Desktop\Subsystems\TRA\ISS-WC\ISS-R-WC.txt';
GMAT ReportFile1.Precision = 16;
GMAT ReportFile1.Add = {DefaultSC.A1Gregorian, DefaultSC.ElapsedDays, DefaultSC.Earth.SMA, DefaultSC.Earth.ECC, DefaultSC.EarthMJ2000Eq.INC, DefaultSC.EarthMJ2000Eq.RAAN, DefaultSC.EarthMJ2000Eq.AOP, DefaultSC.Earth.TA};
GMAT ReportFile1.WriteHeaders = true;
GMAT ReportFile1.LeftJustify = On;
GMAT ReportFile1.ZeroFill = Off;
GMAT ReportFile1.FixedWidth = true;
GMAT ReportFile1.Delimiter = ' ';
GMAT ReportFile1.ColumnWidth = 23;
GMAT ReportFile1.WriteReport = true;

Create XYPlot XYPlot1;
GMAT XYPlot1.SolverIterations = Current;
GMAT XYPlot1.UpperLeft = [ 0 0 ];
GMAT XYPlot1.Size = [ 0 0 ];
GMAT XYPlot1.RelativeZOrder = 0;
GMAT XYPlot1.Maximized = false;
GMAT XYPlot1.XVariable = DefaultSC.ElapsedDays;
GMAT XYPlot1.YVariables = {DefaultSC.Earth.Altitude};
GMAT XYPlot1.ShowGrid = true;
GMAT XYPlot1.ShowPlot = true;


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
Propagate DefaultProp(DefaultSC) {DefaultSC.Earth.Altitude = 100};
FindEvents EclipseLocator1 {Append = false};
FindEvents ContactLocator30 {Append = false};
FindEvents ContactLocator20 {Append = false};
