# Mission Analysis

---------------------
    FOLDER NAMING 
---------------------

In this repository all the results from the mission analysis can be found using GMAT (General Mission Analysis Tool). Each folder represents a different orbit scenario containing the results and the script that was used in order to extract those. 

Folders starting with a number (e.g. "500-BC") represent the results of an SSO at 500 km respectively. Following the number three different abbreviations can be used: 

"...-BC": Represents the Best Case scenario (depending on the orientation of the spacecraft) with 0 degrees misalignment on the yaw and pitch axis. 

"...-NC": Represents the Nominal Case scenario (depending on the orientation of the spacecraft) with 10 degrees misalignment on the yaw and pitch axis. 

"...-WC": Represents the Worst Case scenario (depending on the orientation of the spacecraft) with 20 degrees misalignment on the yaw and pitch axis.

"...-RAAN": Represent the sensitivity analysis of the eclipse time for this perticular orbit with respect to the RAAN parameter.

Some folders can start with the name of the orbit (e.g. "ISS-BC") meaning it contains the script and the results of an ISS launch (Best Case scenario for this case).

---------------------
    RESULTS NAMING
---------------------

Inside each folder at least 6 files can be found:
- The .script file containing the script that was used in GMAT.
- The keplerian elements of the satellite through out the orbit.
- The communication window between the satellite and the ground station (two files one for the S-band and one for the UHF-band)
- The eclipse time of the satellite.
- A plot of the lifetime of the satellite given in time [days] vs altitude [km]. 

All the file names conatin the folder name with some extra abbreviations:

"R-...": Represent the results of the keplerian elements calculation for the entire orbit in each time step. More specifically the Semi-major axis [km], Eccentricity, Inclination [degrees], Right Ascension of Ascending Node [degrees], Argument of Perigee [degrees] and True Anomaly [degrees] are calculated.

"...-C-S": Represents the communication window for the S-band (with a minimum elevantion angle of 20 degrees).

"...-C-UHF": Represents the communication window for the UHF-band (with a minimum elevantion angle of 30 degrees).

"...-E": Represents the eclipse time of the satellite.

For some cases an extra file can be found: 

"...-b": Represents the beta angle of the satellite for the entire orbit.

"...-RAAN-XX": Represents the results of the eclipse time with a initial RAAN at XX degrees (e.g. 500-NC-E-RAAN-120 corresponds to th results of the calculated aclipse time for the SSO at 500 km for the nominal case scenario with an initial RAAN of 120 degrees).